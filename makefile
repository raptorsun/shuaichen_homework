exe/test:calcul.o  EF.o   main.o 
	gcc -o exe/test source/main.o  source/EF.o  source/calcul.o  -Llib -lmat -lm

calcul.o:source/calcul.c 
	gcc -c -g source/calcul.c  -o source/calcul.o
EF.o:source/EF.c  
	gcc -c  -g source/EF.c -o source/EF.o 
main.o:source/main.c
	gcc -c -g source/main.c -o source/main.o 

clean:
	rm exe/test source/calcul.o source/EF.o 
