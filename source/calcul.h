#ifndef calcul_h
#define calcul_h
// declaration des structures//
struct matrice {
  char nom[20];

  char type[20];  // sym,plein
  int n;          // numero de ligne
  int m;          // numero de column
  double **composante;
  struct matrice *next;
};
typedef struct matrice *matrices;

double **allocation_sym(int n);
double **allocation_plein(int m, int n);
struct matrice *creation(int n, int m, char *nom, char *type);
struct matrice *produit(struct matrice *A, struct matrice *B);
void affiche2(struct matrice *A);
void affiche(struct matrice *A);
void destruction(struct matrice *C);
#endif /*calcul.h*/
