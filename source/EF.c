#include "EF.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "calcul.h"
#include "libmat.h"
extern matrices ptr;

extern int nbressort;
extern int nbnoeud;
void aj_elements(struct element *elements, int nbressort) {
  int i;
  for (i = 0; i < nbressort; i++) {
    //[i+1]=

    printf("numero de l'element?\n");
    scanf("%d", &elements[i].numero);
    // elements[i].numero=1;
    printf("ou et le noeud 1 ?\n");
    scanf("%d", &elements[i].noeud[0]);
    // elements[i].noeud[0]=1;
    printf("ou et le noeud 2 ?\n");
    scanf("%d", &elements[i].noeud[1]);
    // elements[i].noeud[1]=2;
    printf("raideur de ressort?\n");
    scanf("%lf", &elements[i].raideur);
    // elements[i].raideur=2;

  }  // ajouter les elements
}
void aj_noeuds(struct noeud *noeuds, int nbnoeud) {
  int i;
  for (i = 0; i < nbnoeud; i++) {
    //[i+1]=

    printf("numero du noeud?\n");
    scanf("%d", &noeuds[i].numero);

    printf("Deplacement(1) ou effort(2) connu ?\n");
    scanf("%d", &noeuds[i].connu);
    if (noeuds[i].connu == 1) {
      printf("Déplacement ?\n");
      scanf("%lf", &noeuds[i].deplacement);
    } else {
      printf("Force?\n");
      scanf("%lf", &noeuds[i].force);
    }
  }
}
struct matrice *aj_matrice_element(struct matrice *K, struct element *elements,
                                   int nbressort) {
  int ielt, i, j;
  if (!strcmp(K->type, "plein")) {
    for (ielt = 0; ielt < nbressort; ielt++) {
      i = elements[ielt].noeud[0] - 1;
      j = elements[ielt].noeud[1] - 1;

      K->composante[i][i] += elements[ielt].raideur;
      K->composante[i][j] -= elements[ielt].raideur;
      K->composante[j][i] -= elements[ielt].raideur;
      K->composante[j][j] += elements[ielt].raideur;
    }
    affiche(K);
  } else {
    for (ielt = 0; ielt < nbressort; ielt++) {
      i = elements[ielt].noeud[0] - 1;
      j = elements[ielt].noeud[1] - 1;

      K->composante[i][i] += elements[ielt].raideur;
      K->composante[j][j] += elements[ielt].raideur;
      if (i > j) {
        K->composante[i][j] -= elements[ielt].raideur;

      } else if (i < j) {
        K->composante[j][i] -= elements[ielt].raideur;
      }
    }
  }
}
struct matrice *aj_matrice_force_deplacement(struct matrice *U,
                                             struct matrice *F,
                                             struct matrice *K, int nbnoeud,
                                             struct noeud *noeuds) {
  int *Uconnus, *Uinconus;
  int nbUconnus;
  int i, j, n, m, a;
  struct matrice *sK, *sU, *sF, *Kr, *B, *Ur, *X;
  nbUconnus = 0;
  for (i = 0; i < nbnoeud; i++) {
    if (noeuds[i].connu == 1) {
      U->composante[i][0] = noeuds[i].deplacement;
      nbUconnus += 1;
    } else
      F->composante[i][0] = noeuds[i].force;
  }
  printf("nbnoeud-nbUconnus est %d", nbnoeud - nbUconnus);

  sK = creation(nbnoeud - nbUconnus, nbnoeud - nbUconnus, "sK", "sym");
  sU = creation(nbnoeud - nbUconnus, 1, "sU", "plein");
  sF = creation(nbnoeud - nbUconnus, 1, "sF", "plein");
  Kr = creation(nbnoeud - nbUconnus, 1, "Kr", "plein");
  B = creation(nbnoeud - nbUconnus, 1, "B", "plein");
  Ur = creation(nbnoeud - nbUconnus, 1, "Ur", "plein");
  X = creation(nbnoeud - nbUconnus, 1, "X", "plein");

  // Sous matrices U plein
  j = 0;
  a = 0;
  Uconnus = malloc((nbnoeud - nbUconnus) * sizeof(int));
  Uinconus = malloc((nbUconnus) * sizeof(int));
  for (i = 0; i < nbnoeud; i++) {
    if (noeuds[i].connu != 1) {
      Uconnus[j] = i;
      j += 1;

    } else {
      Uinconus[a] = i;
      a += 1;
    }
  }

  for (i = 0; i < nbnoeud - nbUconnus; i++)

    for (j = 0; j < nbnoeud - nbUconnus; j++) {
      sK->composante[i][j] += K->composante[Uconnus[i]][Uconnus[j]];
    }
  // affiche(sK); creation de sK

  for (i = 0; i < nbnoeud - nbUconnus; i++) {
    sF->composante[i][0] += F->composante[Uconnus[i]][0];
  }
  // affiche(sF);  creation de sF

  for (i = 0; i < nbnoeud - nbUconnus; i++)
    for (j = 0; j < nbUconnus; j++) {
      Kr->composante[i][j] += K->composante[Uconnus[i]][Uinconus[j]];
    }
  // affiche(Kr); creation de Kr

  for (j = 0; j < nbUconnus; j++) {
    Ur->composante[j][0] += U->composante[Uinconus[j]][0];
  }

  for (i = 0; i < nbnoeud - nbUconnus; i++)
    for (j = 0; j < nbnoeud - nbUconnus; j++) {
      B->composante[i][j] +=
          sF->composante[i][j] - Kr->composante[i][j] * Ur->composante[i][j];
    }
  // affiche(B);

  m = 1;
  n = nbnoeud - nbUconnus;
  solvesym(sK->composante, B->composante, X->composante, n, m);
  // affiche(X);trouve les solution  de X

  for (i = 0; i < nbnoeud - nbUconnus; i++) {
    U->composante[Uconnus[i]][0] += X->composante[i][0];
  }  // affiche(U);

  F = produit(K, U);
  affiche(F);  // TROUVER les solution de f

  for (i = 0; i < nbnoeud - nbUconnus; i++) {
    noeuds[Uconnus[i]].deplacement = U->composante[Uconnus[i]][0];
  }
  for (i = 0; i < nbUconnus; i++) {
    noeuds[Uinconus[i]].force = F->composante[Uinconus[i]][0];
  }
}
void affichenoeud(struct noeud *noeuds, int nbnoeud) {
  int i;
  for (i = 0; i < nbnoeud; i++) {
    printf("noumero de noeud est %d \n", noeuds[i].numero);
    printf("deplacement de noeud est %lf \n", noeuds[i].deplacement);
    printf("force de noeud est %lf \n", noeuds[i].force);
  }  // affiche les noeuds
}
