#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "EF.h"
#include "calcul.h"
#include "libmat.h"
matrices ptr;

int main() {
  int nbnoeud, nbressort;
  struct element *elements;
  struct noeud *noeuds;
  struct matrice *K, *U, *F;

  printf("nombre de ressort?\n");
  scanf("%d", &nbressort);

  elements = malloc(sizeof(struct element) * nbressort);
  aj_elements(elements, nbressort);

  printf("nombre de noeud?\n");
  scanf("%d", &nbnoeud);

  noeuds = malloc(sizeof(struct noeud) * nbnoeud);
  aj_noeuds(noeuds, nbnoeud);

  K = creation(nbnoeud, nbnoeud, "K", "sym");
  U = creation(nbnoeud, 1, "U", "plein");
  F = creation(nbnoeud, 1, "F", "plein");

  aj_matrice_element(K, elements, nbressort);
  aj_matrice_force_deplacement(U, F, K, nbnoeud, noeuds);

  affichenoeud(noeuds, nbnoeud);

  destruction(K);
  destruction(U);
  destruction(F);
}
