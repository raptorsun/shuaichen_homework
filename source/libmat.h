/*

 Prototypes des fonctions de resolution. 
 
 Resoud A*X = B pour A matrice carree de determinant non nul.
 
 Arguments :
 
  A : tableau 2D, de taille n par n
  B : tableau 2D, taille n par m (m colonnes -> mettre 1 pour un vecteur)
  X : tableau 2D, taille n par m 
 
 La fonction solvesym resoud le cas d'une matrice A symetrique 
 en utilisant seulement le triangle superieur de A.
 Dans ce cas, on peut ne stocker que cette partie de A en memoire
 en faisant les decalages d'indices necessaires pour acceder 
 correctement a ces elements.
 
 */

void solvesym(double **A,double **B,double **X,int n,int m);
void solve(double **A,double **B,double **X,int n,int m);