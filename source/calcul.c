#include "calcul.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern matrices ptr;

double **allocation_sym(int n) {
  int i;
  double **A = (double **)malloc((n) * sizeof(double *));

  for (i = 0; i < n; i++) A[i] = (double *)malloc((i + 1) * sizeof(double));
  return A;
}
double **allocation_plein(int m, int n) {
  int i;

  double **A = (double **)malloc((m) * sizeof(double *));

  for (i = 0; i < m; i++) A[i] = (double *)malloc((n) * sizeof(double));
  return A;
}

struct matrice *creation(int n, int m, char *nom, char *type) {
  int i, j;
  struct matrice *C = NULL;

  C = malloc(sizeof(struct matrice));
  strcpy(C->nom, nom);
  strcpy(C->type, type);
  if (!strcmp(type, "sym")) {
    C->m = m;
    C->n = n;
    C->composante = allocation_sym(n);

    printf("%d %d", C->m, C->n);
    /* mise a zero des composantes */
    /* for (i=0;i<n;i++)
        {

         for (j=0;j<m;j++)
            {
              if(j<=i)
                    C->composante[i][j] =0;
            }
        }*/
  } else if (!strcmp(type, "plein")) {
    C->n = n;
    C->m = m;

    C->composante = allocation_plein(n, m);
    /* mise a zero des composantes */

    /* for (i=0;i<n;i++)
         for (j=0;j<m;j++)

      C->composante[i][j] =0;*/
    printf("%d %d", C->m, C->n);
  }
  C->next = ptr;
  ptr = C;
  return C;
}
struct matrice *produit(struct matrice *A, struct matrice *B) {
  int i, j, k;
  struct matrice *C = NULL;
  struct matrice *D = NULL;

  if (A->m == B->n) {
    if (!strcmp(A->type, "plein")) {
      C = creation(A->n, B->m, "C", "plein"); /* creation de la matrice C */
      for (i = 0; i < A->n; i++)
        for (j = 0; j < B->m; j++) {
          C->composante[i][j] = 0;
          for (k = 0; k < A->m; k++)
            C->composante[i][j] += A->composante[i][k] * B->composante[k][j];
        }
    }
    if (!strcmp(A->type, "sym")) {
      D = creation(A->n, A->m, "D", "plein");
      for (i = 0; i < A->n; i++) {
        for (j = 0; j < A->m; j++) {
          D->composante[i][i] = A->composante[i][i];
          D->composante[i][j] = D->composante[j][i] = A->composante[i][j];
        }
      }                                       // creation un nouvaul matrice D
      C = creation(D->n, B->m, "C", "plein"); /* creation de la matrice C */
      for (i = 0; i < D->n; i++)
        for (j = 0; j < B->m; j++) {
          C->composante[i][j] = 0;
          for (k = 0; k < D->m; k++)
            C->composante[i][j] += D->composante[i][k] * B->composante[k][j];
        }
    }
  } else
    printf("Impossible de faire le produit\n");

  return C;
}

void affiche2(struct matrice *A) {
  int i, j;

  printf("\n %s = [\n ", A->nom);
  if (!strcmp(A->type, "sym")) {
    for (i = 0; i < A->n; i++) {
      for (j = 0; j < A->m; j++) printf(" %lf ", A->composante[i][j]);
      printf(" \n ");
    }

  } else {
    for (i = 0; i < A->n; i++)
      for (j = 0; j < A->m; j++) {
        printf(" %lf ", A->composante[i][j]);
      }
  }
  printf("] \n ");
}
void affiche(struct matrice *A) {
  int i, j;

  printf("\n %s = [\n ", A->nom);

  for (i = 0; i < A->n; i++) {
    for (j = 0; j < A->m; j++) printf(" %lf ", A->composante[i][j]);
    printf(" \n ");
  }
  printf("] \n ");
}

void destruction(struct matrice *C) {
  int i;

  for (i = 0; i < C->n; i++) free(C->composante[i]);
  free(C->composante);
  free(C);
}
