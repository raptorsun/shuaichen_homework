#ifndef EF_h
#define EF_h
struct noeud {
  int numero;
  int connu;  // 1=deplacement, 2=force
  double deplacement;
  double force;
};

struct element {
  int numero;
  int noeud[2];
  double raideur;
};
typedef struct noeud *noeuds;
typedef struct element *elements;

void aj_elements(struct element *elements, int nbressort);
void aj_noeuds(struct noeud *noeuds, int nbnoeud);
struct matrice *aj_matrice_element(struct matrice *K, struct element *elements,
                                   int nbressort);
struct matrice *aj_matrice_force_deplacement(struct matrice *U,
                                             struct matrice *F,
                                             struct matrice *K, int nbnoeud,
                                             struct noeud *noeuds);
void affichenoeud(struct noeud *, int);
#endif /*EF.h*/
